# maXbox4
code compiler script studio


Release Notes maXbox 4.2.8.10 Oct 2017 Ocean8 mX4

add 12 units XMLDoc XMLIntf, ADO_Recordset CryptoLib4, XMLW
add 5 Tutors: #50-54 (Microservice, CryptoAPI, maXML)
new switch: DEP Data Execution Prevention
https://maxbox4.wordpress.com/

1273 uPSI_CromisStreams, (TStreamStorage)
1274 unit uPSI_Streams,
1275 uPSI_BitStream,
1276 uPSI_UJSONFunctions.pas
1277 uPSI_uTPLb_BinaryUtils.pas
1278 unit uPSI_USha256.pas //PascalCoin Blockchain
1279 uPSI_uTPLb_HashDsc.pas
1280 uPSI_uTPLb_Hash.pas
1281 SIRegister_Series(X); (uPSI_Series) //4.2.6.10
1282 unit uPSI_UTime; (UTime); uPSI_mimeinln2;
1283 uPSI_uTPLb_StreamCipher.pas
1284 uPSI_uTPLb_BlockCipher.pas
1285 uPSI_uTPLb_Asymetric.pas
1286 uPSI_uTPLb_CodecIntf.pas
1287 uPSI_uTPLb_Codec.pas
1288 uPSI_ADOInt.pas
1289 uPSI_MidasCon.pas
1290 uPSI_XMLDoc.pas
1291 uPSI_XMLIntf.pas
1292 uPSI_ProxyUtils.pas

Totals of Function Calls: 31306
SHA1: of 4.2.8.10 C3FEF1DF49C22A312A249D5A59078CEE1E67E774
CRC32: 1D4C6F1F 26.9 MB (28,293,072 bytes)

